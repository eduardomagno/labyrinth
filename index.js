
  
  const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
  ];

  

  const createMap = () =>{
    let idCount = 0;


    for(let x = 0; x < map.length; x++){ 
      for(let i = 0; i < map[x].length; i++){ 
        const caracter = map[x][i]
        const wall  = document.createElement('div')
        wall.className = 'box'
        switch(caracter){
          case 'W':
            wall.id = `w${idCount}`
            wall.classList = 'box blue'
            break

          case 'S':
            wall.id = 'start'
            wall.classList =' box start'
            break

          case 'F':
            wall.id = 'finish'
            wall.classList = 'box finish'
            break

          default:
            wall.classList = 'box snow'
            break
          }
          idCount++
        document.getElementById('container').appendChild(wall)
      }
    }

    

  }
  

  const player = () => {
    const player = document.createElement('div')
    player.id = 'player'
    document.querySelector('.start').appendChild(player)
  }
  
  const cellsMap = () =>{
    const cells = []
    const childrens = document.getElementById('container').children
    for (let i = 0; i < childrens.length; i ++){
      if (childrens[i].classList.contains('snow')){
        childrens[i].id = i
        cells.push(childrens[i])
      }
    }
    return cells
  }

  const winGame =  () =>{
    const win = document.getElementById('win')
    win.textContent = 'VOCÊ GANHOU, PARABÉNS!!!'
    
    }

  const movePlayer = (event) =>{
    const player = document.getElementById('player')
      
    
      const keyName = event.key
      const cell = player.parentNode
      const nextCell = cell.nextSibling
      const previousCell = cell.previousSibling
      
      switch(keyName){
          case 'ArrowUp':
            for( let i = 0; i < cellsMap().length; i++){
              const idOffAllCells = cellsMap()[i].id
              const idUCell = parseInt(cell.id)
              const subtraction = idUCell - 21
              const idUpCell = subtraction.toString()
              if (idOffAllCells === idUpCell){
                document.getElementById(idUpCell).appendChild(player)
              } 
            }
            break
          
          case 'ArrowDown':
            for (let i = 0; i < cellsMap().length; i++){
              const idOffAllCells = cellsMap()[i].id
              const idDCell = parseInt(cell.id)
              const sum = idDCell + 21
              const idDownCell = sum.toString()
              if (idOffAllCells === idDownCell){
                document.getElementById(idDownCell).appendChild(player)
              }
            }
            break
          
          case 'ArrowLeft':
            if (previousCell.classList.contains('snow') ){
              previousCell.appendChild(player)
            }
            if (previousCell.classList.contains('finish')){
              previousCell.appendChild(player)
              }
            break
          
          case 'ArrowRight':
            if(nextCell.classList.contains('snow')){
                nextCell.appendChild(player)
            }
            if (nextCell.classList.contains('finish')){
              nextCell.appendChild(player)
              document.removeEventListener('keydown', movePlayer,false)
              winGame()
            }
            break
          
          default:
            break
        }
    
  }

  

  const reset = () =>{
   const player = document.getElementById('player')
   const start = document.getElementById('start')
   const win = document.getElementById('win')

   if (player.parentElement.id === 'finish'){
    start.appendChild(player)
    win.textContent = ''
   }

   if (player.parentElement.classList.contains('snow')){
     start.appendChild(player)
   }

   document.addEventListener('keydown', movePlayer,false)
  }

  const initGame = () =>{
    const button = document.getElementById('reset')
    
    createMap()
    
    player()
    
    document.addEventListener('keydown', movePlayer,false)
    button.addEventListener('click', reset,false)
  }

  initGame()

 